import NextHead from "next/head";
import {string} from "prop-types";
import * as React from "react";

const defaultDescription = "Serwis podróżniczy dla każdego!";
const defaultKeywords = "podróże, ciekawe miejsca, ciekawe miasta";
const defaultOGURL = "";
const defaultOGImage = "";

class Head extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    function gtag() {
      window.dataLayer.push(arguments)
    }

    window.dataLayer = window.dataLayer || [];

    gtag("js", new Date());
    gtag("config", "UA-30426845-2");
  }

  render() {
    const props = this.props;

    return (
      <NextHead>
        <meta charset="UTF-8"/>
        <title>{props.title || ""}</title>
        <meta charset="utf-8"/>
        <meta name="description" content={props.description || defaultDescription}/>
        <meta name="kewords" content={props.kewords || defaultKeywords}/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="apple-touch-icon" href="/static/touch-icon.png"/>
        <link rel="mask-icon" href="/static/favicon-mask.svg" color="#49B882"/>
        <link rel="icon" href="/static/favicon.ico"/>
        <meta property="og:url" content={props.url || defaultOGURL}/>
        <meta property="og:title" content={props.title || ""}/>
        <meta property="og:description" content={props.description || defaultDescription}/>
        <meta name="twitter:site" content={props.url || defaultOGURL}/>
        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:image" content={props.ogImage || defaultOGImage}/>
        <meta property="og:image" content={props.ogImage || defaultOGImage}/>
        <meta property="og:image:width" content="1200"/>
        <meta property="og:image:height" content="630"/>
        <meta name="google-site-verification" content="vRoQdcXSnAfRMATZrSF7kPCefQtHb7fxW3h7weKGNBU"/>

        <link rel="stylesheet" href="/static/semantic-ui-css/semantic.min.css"/>
        <link rel="stylesheet" href="/static/index.css"/>

        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-30426845-2"></script>

      </NextHead>
    );
  }

}

;

Head.propTypes = {
  title: string,
  description: string,
  url: string,
  ogImage: string
};

export default Head
