import React from 'react'
import {Feed, Icon} from 'semantic-ui-react'

const FeedExampleBasic = () => (
  <Feed>
    <Feed.Event>
      <Feed.Label>
        <img src='/static/elliot.jpg'/>
      </Feed.Label>
      <Feed.Content>
        <Feed.Summary>
          <Feed.User>Jan Nowak</Feed.User> dodał nową opinię
          <Feed.Date>1 godzinę temu</Feed.Date>
        </Feed.Summary>
        <Feed.Meta>
          <Feed.Like>
            <Icon name='like'/>
            4 Aprobaty
          </Feed.Like>
        </Feed.Meta>
      </Feed.Content>
    </Feed.Event>

    <Feed.Event>
      <Feed.Label image='/static/helen.jpg'/>
      <Feed.Content>
        <Feed.Summary>
          <a>Ewa Matyska</a> dodała <a>2 nowe zdjęcia</a>
          <Feed.Date>4 dni temu</Feed.Date>
        </Feed.Summary>
        <Feed.Meta>
          <Feed.Like>
            <Icon name='like'/>
            1 Aprobata
          </Feed.Like>
        </Feed.Meta>
      </Feed.Content>
    </Feed.Event>

    <Feed.Event>
      <Feed.Label image='/static/jenny.jpg'/>
      <Feed.Content>
        <Feed.Summary date='2 dni temu' user='Asia Kwiat' content=' zaktualizowała swoją planowaną podróż'/>
        <Feed.Meta>
          <Feed.Like>
            <Icon name='like'/>
            8 Aprobat
          </Feed.Like>
        </Feed.Meta>
      </Feed.Content>
    </Feed.Event>

    <Feed.Event>
      <Feed.Label image='/static/joe.jpg'/>
      <Feed.Content>
        <Feed.Summary>
          <a>Ludwik Jabłczyński</a> dodał posta na swój profil
          <Feed.Date>3 dni temu</Feed.Date>
        </Feed.Summary>
        <Feed.Extra text>
          Gdzie najlepiej podróżować w zimę?
        </Feed.Extra>
        <Feed.Meta>
          <Feed.Like>
            <Icon name='like'/>
            5 Aprobat
          </Feed.Like>
        </Feed.Meta>
      </Feed.Content>
    </Feed.Event>
  </Feed>
);

export default FeedExampleBasic