import {Button, Dropdown, Menu} from 'semantic-ui-react'
import Link from "next/link"
import React from "react";

const Nav = () => (
  <Menu fluid={true} stackable={true}>
    <Link href="/">
      <Menu.Item header>Strona Główna</Menu.Item>
    </Link>
  </Menu>
);

export default Nav;
