import * as React from "react";
import {PlaceItem} from "./place-item";

export class PlaceList extends React.Component {
  render() {

    return this.props.itemList.map((item, index) => {
      return <PlaceItem key={index} item={item} />;
    })
  }
}