import * as React from "react";
import {Container, Image, Menu} from "semantic-ui-react";
import Link from "next/link"

export class MenuComponent extends React.Component {
  render() {
    return (
      <Menu fixed='top' inverted>
        <Container>
          <Link href="/">
            <Menu.Item as='a' header>Travel4You</Menu.Item>
          </Link>
        </Container>
      </Menu>
    );
  }
}