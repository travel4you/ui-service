import * as React from "react";
import {Item, Rating} from "semantic-ui-react";
import {buildUrl, slugify} from "./utils";
import Link from "next/link"

export class PlaceItem extends React.Component {
  render() {
    const item = this.props.item;
    const slugifiedName = slugify(item.name);
    const placeDetailsUrl = buildUrl('/place', {id: item.id, name: item.name});
    const placeDetailsNiceUrl = ['/miejsca', item.id, slugifiedName].join('/');

    return (
      <Item>
        <Item.Image size="small" src={item.imageUrlAdditional} alt={item.name}/>

        <Item.Content>
          <Link href={placeDetailsUrl} as={placeDetailsNiceUrl}>
            <Item.Header as="a">{item.name}</Item.Header>
          </Link>
          <Rating icon="star" defaultRating={item.avgRate} maxRating={5} disabled/>
          <Item.Description>
            {item.shortDescription}
          </Item.Description>
        </Item.Content>
      </Item>
    );
  }
}