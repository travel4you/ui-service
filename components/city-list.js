import * as React from "react";
import {slugify} from "../components/utils";
import Link from "next/link"
import {CityItem} from "./city-item";

export class CityList extends React.Component {
  render() {
    return this.props.itemList.map((item, index) => {
      return (
        <Link key={index}
              href={"/city?id=" + item.id + "&name=" + slugify(item.name)}
              as={"/miasta/" + item.id + "/" + slugify(item.name)}
        >
          <CityItem item={item}/>
        </Link>
      );
    })
  }
}