import * as React from "react";
import {Button, Container, Header, Icon, Segment} from "semantic-ui-react";
import SearchComponent from "./search";

export class Lead extends React.Component {
  onSearchClick() {
    this.refs.search.handleOpen();
  }

  render() {
    const style = {};


    return (
      <div>
        <SearchComponent ref="search"/>

        <Segment textAlign="center" className="lead-photo" vertical style={style}>

          <Container text>
            <Header as="h1" content="Ciekawe miejsce?" inverted className="lead-header"/>
            <Header as="h2" content="Napewno coś znajdziesz :)" inverted className="lead-subheader"/>
            <Button primary size="huge" onClick={this.onSearchClick.bind(this)}>
              <span>Szukaj</span>
              <Icon name="search" className="right"/>
            </Button>
          </Container>

        </Segment>
      </div>
    );
  }
}