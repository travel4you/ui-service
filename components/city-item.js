import * as React from "react";
import {Card, Image} from "semantic-ui-react";
import Link from "next/link"
import {buildUrl, slugify} from "./utils";

export class CityItem extends React.Component {
  render() {
    const item = this.props.item;
    const slugifiedName = slugify(item.name);
    const placeDetailsUrl = buildUrl('/city', {id: item.id, name: item.name});
    const placeDetailsNiceUrl = ['/miasta', item.id, slugifiedName].join('/');

    return (
      <Card fluid={false} centered={true}>
        <Image src={item.imageUrl} alt={item.name}/>
        <Card.Content>
          <Link href={placeDetailsUrl} as={placeDetailsNiceUrl}>
            <Card.Header content={item.name} as="a"/>
          </Link>
        </Card.Content>
      </Card>
    );
  }
}