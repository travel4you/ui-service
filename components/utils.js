import fetch from 'isomorphic-unfetch';


export const hostUrl = "http://a225b7bd5007611e8a2ba0235097f2aa-843915674.eu-central-1.elb.amazonaws.com";

export function fetchCityList() {
  return fetch(`${hostUrl}/backend/city-service/api/v1/city/`)
    .then(r => r.json());
}

export function fetchCityDetails(id) {
  return fetch(`${hostUrl}/backend/city-service/api/v1/city/${id}`)
    .then(r => r.json());
}


export function fetchPlaceListByCityId(cityId) {
  return fetch(`${hostUrl}/backend/place-service/api/v1/place?cityId=${cityId}`)
    .then(r => r.json());
}

export function fetchPlaceDetails(id) {
  return fetch(`${hostUrl}/backend/place-service/api/v1/place/${id}`)
    .then(r => r.json());
}

export function fetchPlaceList() {
  return fetch(`${hostUrl}/backend/place-service/api/v1/place/`)
    .then(r => r.json());
}

export function buildUrl(endpoint, queryParams) {
  const builtParams = Object
    .keys(queryParams)
    .map((queryParamName) => `${queryParamName}=${queryParams[queryParamName]}`)
    .join('&');

  return endpoint + '?' + builtParams;
}

export function slugify(input) {
  return input
    .toLowerCase()
    .trim()
    .replace(/\s+/g, '-')
    .replace(/[żź]/g, 'z')
    .replace(/ó/g, 'o')
    .replace(/ł/g, 'l')
    .replace(/ą/g, 'a')
    .replace(/ń/g, 'n')
    .replace(/ć/g, 'c')
    .replace(/ś/g, 's')
    .replace(/ę/g, 'e');
}