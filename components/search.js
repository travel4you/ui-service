import * as React from "react";
import {Container, Dimmer, Header, Input} from "semantic-ui-react";

export default class SearchComponent extends React.Component {

  handleOpen = () => this.setState({active: true});
  handleClose = () => this.setState({active: false});

  constructor(props) {
    super(props);
    this.state = {
      active: false
    }
  }

  render() {
    const {active} = this.state;

    return (
      <Dimmer
        active={active}
        onClickOutside={this.handleClose}
        page
      >
        <Container textAlign='center'>
          <Header color='teal'>
          </Header>
          <Input icon='search' placeholder='Szukaj...' size='huge'/>
        </Container>
      </Dimmer>
    )
  }
}