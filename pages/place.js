import * as React from "react";
import Head from "../components/head";
import {Grid, Image, List, Rating, Responsive, Segment} from "semantic-ui-react";
import Nav from "../components/nav";
import {fetchPlaceDetails} from "../components/utils";

export default class PlaceComponent extends React.Component {
  static async getInitialProps(props) {
    const query = props.query;
    const fetchedDetails = await fetchPlaceDetails(query.id);

    return {
      ...query,
      fetchedDetails
    };
  }

  render() {
    const {name, id, fetchedDetails} = this.props;

    return (
      <Responsive>
        <Head title={name + " - informacje o miejscu"}
              description={"Ciekawe informacje o - " + name}/>

        <Nav/>

        <Segment>
          <Grid container stackable>
            <Grid.Row>
              <Grid.Column floated="left" width={7}>
                <Image src={fetchedDetails.imageUrlAdditional}
                       alt={fetchedDetails.name}
                       size="large"
                       bordered={true}
                       rounded={true}/>
              </Grid.Column>
              <Grid.Column width={9}>
                <List>
                  <List.Item>
                    <List.Header>Opis</List.Header>
                    {fetchedDetails.shortDescription}
                  </List.Item>
                  <List.Item>
                    <List.Header>Średnia ocena</List.Header>
                    <Rating icon="star" defaultRating={fetchedDetails.avgRate} maxRating={5} disabled/>
                  </List.Item>
                  <List.Item>
                    <List.Header>Szerokość geograficzna</List.Header>
                    {fetchedDetails.latitude}
                  </List.Item>
                  <List.Item>
                    <List.Header>Długość geograficzna</List.Header>
                    {fetchedDetails.longitude}
                  </List.Item>
                </List>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>

      </Responsive>
    )
  }
}