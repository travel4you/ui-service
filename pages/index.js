import Head from "../components/head"

import {fetchCityList, fetchPlaceList} from "../components/utils";

import FeedExampleBasic from "../components/feeds";
import {CityList} from "../components/city-list";
import * as React from "react";
import {Card, Divider, Grid, Header, Item, Responsive, Segment} from "semantic-ui-react";
import {Lead, LeadComponent} from "../components/lead";
import {PlaceList} from "../components/place-list";
import Nav from "../components/nav";

export default class IndexComponent extends React.Component {

  static async getInitialProps({req}) {
    const fetchedCityList = await fetchCityList();
    const fetchedPlaceList = await fetchPlaceList();

    return {
      fetchedCityList,
      fetchedPlaceList
    }
  }

  render() {
    const fetchedCityList = this.props.fetchedCityList;
    const fetchedPlaceList = this.props.fetchedPlaceList;

    return (
      <Responsive>
        <Head title="Strona Główna - podróże, ciekawe miejsca i miasta"
              description="Serwis podróżniczy - zobacz ciekawe miejsca, ciekawe miasta, zaplanuj podróże!"
              kewords="podróże, ciekawe miejsca, ciekawe miasta"/>

        <Nav/>
        <Lead/>

        <Grid columns={1} stackable={true} doubling={true} relaxed={true}>
          <Grid.Row columns={2}>
            <Grid.Column>
              <Segment>
                <Header as="h2" content="Najciekawsze Miejsca!" icon="marker "/>
                <Divider/>

                <Item.Group>
                  <PlaceList itemList={fetchedPlaceList}/>
                </Item.Group>

              </Segment>
            </Grid.Column>

            <Grid.Column>
              <Segment>
                <Header as="h2" content="Najciekawsze Miasta!" icon="camera "/>

                <Divider/>

                <Card.Group itemsPerRow={2} stackable={true} doubling={true}>
                  <CityList itemList={fetchedCityList}/>
                </Card.Group>
              </Segment>

              <Segment>
                <Header as="h2" content="Aktywności użytkowników" icon="talk"/>

                <Divider/>

                <FeedExampleBasic></FeedExampleBasic>
              </Segment>

            </Grid.Column>

          </Grid.Row>
        </Grid>
      </Responsive>
    )
  }
}