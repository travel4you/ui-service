import * as React from "react";
import Head from "../components/head";
import {Divider, Grid, Header, Image, Item, List, Responsive, Segment} from "semantic-ui-react";
import {fetchCityDetails, fetchPlaceListByCityId} from "../components/utils";
import {PlaceList} from "../components/place-list";
import Nav from "../components/nav";


export default class CityComponent extends React.Component {
  static async getInitialProps(props) {
    const query = props.query;
    const fetchedDetails = await fetchCityDetails(query.id);
    const fetchedPlaceList = await fetchPlaceListByCityId(query.id);

    return {
      ...query,
      fetchedDetails,
      fetchedPlaceList
    };
  }

  render() {
    const {name, id, fetchedDetails, fetchedPlaceList = []} = this.props;

    debugger;

    return (
      <Responsive>
        <Head title={name + " - informacje o mieście"}
              description={"Wybierasz się do miasta - " + name + "? Zobacz najciekawsze informacje!"}/>

          <Nav />

          <Segment>
            <Grid container stackable>
              <Grid.Row>
                <Grid.Column>
                  <Header as="h2" content={fetchedDetails.name} />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column floated="left" width={7}>
                  <Image src={fetchedDetails.imageUrl}
                         alt={fetchedDetails.name}
                         size="large"
                         bordered={true}
                         rounded={true}/>
                </Grid.Column>
                <Grid.Column width={9}>
                  <List>
                    <List.Item>
                      <List.Header>Opis</List.Header>
                      {fetchedDetails.description}
                    </List.Item>
                    <List.Item>
                      <List.Header>Populacja</List.Header>
                      {fetchedDetails.population}
                    </List.Item>
                    <List.Item>
                      <List.Header>Terytorium</List.Header>
                      {fetchedDetails.territory}
                    </List.Item>
                    <List.Item>
                      <List.Header>Szerokość geograficzna</List.Header>
                      {fetchedDetails.latitude}
                    </List.Item>
                    <List.Item>
                      <List.Header>Długość geograficzna</List.Header>
                      {fetchedDetails.longitude}
                    </List.Item>
                  </List>
                </Grid.Column>
              </Grid.Row>

              <Grid.Row>
                <Grid.Column>
                  <Header as="h2" content={"Najciekawsze miejsca w mieście " + fetchedDetails.name } />
                  <Divider />

                  <Item.Group>
                    <PlaceList itemList={fetchedPlaceList}/>
                  </Item.Group>
                </Grid.Column>
              </Grid.Row>
            </Grid>

          </Segment>

      </Responsive>
    )
  }
}