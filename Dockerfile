FROM node:6.9.5-alpine
MAINTAINER Piotr Pażuchowski <piotr.pazuchowski@gmail.com>

ADD . /app
WORKDIR /app

RUN npm install
RUN npm run build

CMD ["npm", "start"]

EXPOSE 3000